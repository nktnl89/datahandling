package Dates.Services;

import Dates.Interfaces.AgeServiceInterface;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class AgeService implements AgeServiceInterface {

    @Override
    public void getAgeNow(LocalDateTime dateBirthDay) {
        LocalDateTime currentMoment = LocalDateTime.now();
        Period ageDate = Period.between(dateBirthDay.toLocalDate(), currentMoment.toLocalDate());
        Duration ageTime = Duration.between(dateBirthDay.toLocalTime(), currentMoment.toLocalTime());

        long totalMonths = ChronoUnit.MONTHS.between(dateBirthDay, currentMoment);
        long totalDays = ChronoUnit.DAYS.between(dateBirthDay, currentMoment);
        long totalHours = ChronoUnit.HOURS.between(dateBirthDay, currentMoment);
        long totalMinutes = ChronoUnit.MINUTES.between(dateBirthDay, currentMoment);
        long totalSeconds = ChronoUnit.SECONDS.between(dateBirthDay, currentMoment);

        System.out.println("------------------------Задача 1.1------------------------");
        System.out.println(ageDate.getYears() + " years");
        System.out.println(ageDate.getMonths() + " months (total " + totalMonths + ")");
        System.out.println(ageDate.getDays() + " days (total " + totalDays + ")");
        System.out.println(ageTime.toHours() + " hours (total " + totalHours + ")");
        System.out.println(ageTime.toMinutes() - ageTime.toHours() * 60 + " minutes (total " + totalMinutes + ")");
        System.out.println(ageTime.toMillis()/1000 -((ageTime.toMinutes() - ageTime.toHours() * 60)*60)-ageTime.toHours()*60*60 + " seconds (total " + totalSeconds + ")");

    }
}
