package Dates.Services;

import Dates.Interfaces.AgeServiceInterface;

import java.time.LocalDateTime;

public class DemoAgeService {
    public void startDemo(){
        AgeServiceInterface ageService = new AgeService();
        LocalDateTime birthDay = LocalDateTime.of(1989, 8, 15, 2, 0, 0);
        ageService.getAgeNow(birthDay);
    }

}
