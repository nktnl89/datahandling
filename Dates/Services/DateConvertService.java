package Dates.Services;

import Dates.Interfaces.DateConvertServiceInterface;

import javax.xml.bind.SchemaOutputResolver;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

public class DateConvertService implements DateConvertServiceInterface {

    @Override
    public void getDateConverted(String date) {
        System.out.println("------------------------Задача 1.3------------------------");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy hh:mm:ss a", Locale.ENGLISH);
        LocalDateTime localDateTime = LocalDateTime.parse(date,dateFormatter);
        DateTimeFormatter dateFormatterResult = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        System.out.println(localDateTime.format(dateFormatterResult));
    }

    @Override
    public void getDateConvertedToIzhevsk(String date) {
        System.out.println("------------------------Задача 1.3*-----------------------");
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        ZonedDateTime izhDateTime = zonedDateTime.withZoneSameInstant(ZoneId.of("GMT+3"));
        System.out.println(izhDateTime);

    }
}
