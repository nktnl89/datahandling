package Dates.Services;

import Dates.Interfaces.DateConvertServiceInterface;

public class DemoDateConvert {
    public void startDemo(){
        DateConvertServiceInterface dateConvertService = new DateConvertService();
        dateConvertService.getDateConverted("Friday, Aug 12, 2016 12:10:56 PM");
        dateConvertService.getDateConvertedToIzhevsk("2016-08-16T10:15:30+08:00");
    }
}
