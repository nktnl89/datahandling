package Dates.Services;

import Dates.Interfaces.DatesDifferenceInterface;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DatesDifferenceService implements DatesDifferenceInterface {

    @Override
    public void getDatesDifference(String date1, String date2) {

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        long daysBetween = ChronoUnit.DAYS.between(LocalDate.parse(date1,dateFormatter),LocalDate.parse(date2,dateFormatter));
        System.out.println("------------------------Задача 1.2------------------------");
        System.out.println(daysBetween+" days");

    }
}
