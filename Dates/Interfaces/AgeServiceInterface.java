package Dates.Interfaces;

import java.time.LocalDateTime;

public interface AgeServiceInterface {
    public void getAgeNow(LocalDateTime dateBirthDay);
}
