package Dates.Interfaces;

public interface DateConvertServiceInterface {
    public void getDateConverted(String date);
    public void getDateConvertedToIzhevsk(String date);
}
