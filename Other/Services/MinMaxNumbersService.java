package Other.Services;

import Other.Interfaces.MinMaxNumbersInterface;

public class MinMaxNumbersService implements MinMaxNumbersInterface {
    @Override
    public void getMinMaxNumbers(int num1, int num2, int num3) {
        System.out.println("------------------------Задача 2.3------------------------");
        System.out.println("max: "+Math.max(Math.max(num1,num2),num3));
        System.out.println("min: "+Math.min(Math.min(num1,num2),num3));
    }
}
