package Other.Services;

import Other.Interfaces.CircleAreaServiceInterface;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CircleAreaService implements CircleAreaServiceInterface {
    @Override
    public void gerCircleArea(int radius) {
        System.out.println("------------------------Задача 2.1------------------------");
        BigDecimal area = BigDecimal.valueOf(Double.valueOf(radius*radius)*Math.PI);
        System.out.format("%.50f%n",area);
    }
}
