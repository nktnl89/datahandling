package Other.Services;

import Other.Interfaces.NumbersSumInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class NumberSumService implements NumbersSumInterface {
    @Override
    public void getEqualSumNumbers(String num1, String num2, String sum) {
        System.out.println("------------------------Задача 2.2------------------------");

        Double num1Double = Double.parseDouble(num1);
        Double num2Double = Double.parseDouble(num2);
        Double sumDouble = Double.parseDouble(sum);
        if (num1Double + num2Double == sumDouble) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
