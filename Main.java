import Dates.Services.DemoAgeService;
import Dates.Services.DemoDateConvert;
import Dates.Services.DemoDatesDifference;
import Other.Services.DemoCircleArea;
import Other.Services.DemoMinMaxNumbersService;
import Other.Services.DemoNumberSumService;
import Strings.Services.DemoDivideStringService;
import Strings.Services.DemoIzhevskPhoneService;
import Strings.Services.DemoMaxMinLengthWordService;

public class Main {

    public static void main(String[] args) {
        DemoAgeService demoAgeService = new DemoAgeService();
        demoAgeService.startDemo();

        DemoDatesDifference demoDatesDifference = new DemoDatesDifference();
        demoDatesDifference.startDemo();

        DemoDateConvert demoDateConvert = new DemoDateConvert();
        demoDateConvert.startDemo();

        DemoCircleArea demoCircleArea = new DemoCircleArea();
        demoCircleArea.startDemo();

        DemoNumberSumService demoNumberSum = new DemoNumberSumService();
        demoNumberSum.startDemo();

        DemoMinMaxNumbersService demoMinMaxNumbers = new DemoMinMaxNumbersService();
        demoMinMaxNumbers.startDemo();

        DemoDivideStringService demoDivideString = new DemoDivideStringService();
        demoDivideString.StartDemo();

        DemoMaxMinLengthWordService demoMaxMinLengthWord = new DemoMaxMinLengthWordService();
        demoMaxMinLengthWord.startDemo();

        DemoIzhevskPhoneService demoIzhevskPhone = new DemoIzhevskPhoneService();
        demoIzhevskPhone.startDemo();
    }
}
