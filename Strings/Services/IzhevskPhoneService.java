package Strings.Services;

import Strings.Interfaces.IzhevskPhoneServiceInterface;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IzhevskPhoneService implements IzhevskPhoneServiceInterface {
    @Override
    public void getIzhevskPhoneNumbers(String input) {
        System.out.println("------------------------Задача 4.3------------------------");
        StringBuilder output = new StringBuilder();
        Pattern izhevskCodePattern = Pattern.compile("3412\\)|3412");
        Pattern numbers = Pattern.compile("\\d|-");
        String[] dividedString = izhevskCodePattern.split(input);
        for (String symbols : dividedString) {
            if(symbols.length()>=6){
                Matcher phoneNumber = numbers.matcher(symbols);
                while (phoneNumber.find()){
                    output.append(phoneNumber.group());
                }
            }
        }
        System.out.println(output.toString());

    }
}
