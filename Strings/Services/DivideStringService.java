package Strings.Services;

import Strings.Interfaces.DivideStringServiceInterface;

import java.util.regex.Pattern;

public class DivideStringService implements DivideStringServiceInterface {
    @Override
    public void getDividedString(String input) {
        System.out.println("------------------------Задача 4.1------------------------");
        Pattern pattern = Pattern.compile("\\p{Punct}|\\s");
        String[] dividedString = pattern.split(input);
        for (String word : dividedString) {
            if (word.trim().length() != 0) {
                System.out.print((word.toLowerCase()).trim() + "\n");
            }
        }
    }
}
