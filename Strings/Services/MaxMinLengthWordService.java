package Strings.Services;

import Strings.Interfaces.MaxMinLengthWordInterface;

import java.util.regex.Pattern;

public class MaxMinLengthWordService implements MaxMinLengthWordInterface {
    @Override
    public void getMaxMinLengthWord(String input) {
        System.out.println("------------------------Задача 4.2------------------------");
        Pattern pattern = Pattern.compile("\\p{Punct}|\\s");
        String[] dividedString = pattern.split(input);
        StringBuilder wordsString = new StringBuilder();
        for (String word : dividedString) {
            if (word.trim().length() != 0) {
                wordsString.append((word.toLowerCase()).trim());
                wordsString.append(",");
            }
        }
        String[] wordsWithoutSpaces = pattern.split(wordsString);
        String maxWord = null;
        String minWord = null;
        int maxlength=0;
        int minLength = wordsWithoutSpaces.length;
        for (String word : wordsWithoutSpaces){
            if (Math.max(maxlength,word.length())>maxlength){
                maxlength = word.length();
                maxWord = word;
            }
            if (Math.min(minLength,word.length())<minLength){
                minLength = word.length();
                minWord = word;
            }
        }
        System.out.println("min word: "+minWord);
        System.out.println("max word: "+maxWord);
    }
}
